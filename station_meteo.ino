// Sensor v2.0 for ESP32 Board 
// By PopSchool Willems P1 
// CC-BY-SA 3.0 December 2019


// Include Network libraries :
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
WiFiMulti WiFiMulti;

//Include DHT & settings : 
#include "DHT.h"
#define DHTPIN 25 //identifie la sortie du capteur
#define DHTTYPE DHT11 //identifie le type de capteur
DHT sensor(DHTPIN, DHTTYPE);

#include <ArduinoJson.h>

// Settings  : 
#define LED_OK 4 // Led for OK return
#define LED_HS 22 // Led for BAD return
#define POPSCHOOL 1 // Led for OK return
#include "config.h"
// include should contain : 
// #define myAPIKEY  "your OWM API KEY"
// #define mySSID  ""
// #define myPASSWORD  ""
// #define NODERED_HOST "IP / DNS for NodeRed " without port 
// #define GROUP "" // Group Number (01,...) 
// #define LABEL "" //a label sent to nodered (?)

int main_delay = 5000; // Delay for mainloop  (between queries)

//Allocate memory for JSON 
StaticJsonDocument<1500> json_return;


// SETUP VOID
void setup() 
{
  // LED PINS
  pinMode(LED_OK, OUTPUT);
  pinMode(LED_HS, OUTPUT);
  
  // Starting Serial Communication 
  Serial.begin(115200);
  delay(10);
  
  // Starting Wireless connection
  wifi_connect();
  
  // Starting DHT sensor
  sensor.begin();
}

void send_data() { 
  //used to Send  collected data to NodeRed server
  Serial.println("== sending data to NodeRed... ==");
  float h = sensor.readHumidity();
  float t = sensor.readTemperature();
  Serial.print("H:");
  Serial.print(h);
  Serial.print(",");
  Serial.print("T:");
  Serial.println(t);
  //URL on which Nodered listen (HTTP In)
  String url = "/sensor/g" + String(GROUP) + "?temp=" + String(t) + "&hygro=" + String(h) + "&label=";
  // Initiating the query : 
  int coderet = web_query("http://" + String(NODERED_HOST) + ":1880" + String(url), json_return);
  if (coderet>0){
    Serial.println("Data Sent successfully.");
  } else {
    Serial.println("[ERR] Data NOT Sent.");
  }
}


void get_actions() {
  // used to get actions to do from the NodeRed Server 
  Serial.println("== Get Actions ==");
  // Way to join  the server : 
  String host = String(NODERED_HOST) + ":1880";
  //URL on which Nodered listen (HTTP In)
  String url = "/get_actions";
  // Initiating the Query : 
  int coderet  = web_query("http://"+host+url,json_return);
  
  // if an error occured, just leave  :
  if (coderet<=0){
    Serial.println("Query ERROR.");
    return();
  } 
  // If it's ok, manage actions : 
  if (json_return["porte"] == 1) {
    Serial.println("OUVERTURE PORTE");
  }
  if (json_return["porte"] == 0) {
    Serial.println("FERMETURE PORTE");
  }
}


void get_data(){
  // used to bring extra data from Open Weather Map : 
  Serial.println("== get OWM data ==");
  // Host :
  String host = "api.openweathermap.org";
  // Url for API : 
  String url = "/data/2.5/forecast?q=willems,fr&units=metric&cnt=1&appid=" + String(myAPIKEY);
  // Initiating Query : 
  int coderet= web_query( "http://"+ host + url ,json_return);
    // If we received something (error >0)
  if (coderet>0){
      float Wspeed = json_return["list"][0]["wind"]["speed"];
      float Wdir = json_return["list"][0]["wind"]["deg"];
      Serial.print("Wind speed (m/s)==>");
      Serial.println (Wspeed);
      Serial.print("Wind dir (°)==>");
      Serial.println (Wdir);
  }
} 


void leds_ok(){
  // Widget to turn on only OK led
  digitalWrite(LED_OK, HIGH);
  digitalWrite(LED_HS, LOW);
}

void leds_hs(){
    // Widget to turn on only BAD  led
  digitalWrite(LED_OK,LOW);
  digitalWrite(LED_HS, HIGH);
}


void loop() 
{
  // main loop
  send_data();
  get_data();
  get_actions();
  delay(main_delay);
}


int  web_query(String URL,StaticJsonDocument<1500>  &json_return) {
  // Generic funciton to manage Web Queries : 
  HTTPClient http;
  //host to contact
  http.begin("http://" +URL);
  // Initiatig query : 
  int retCode = http.GET();
  Serial.print("[HTTP] Return code =");
  Serial.println(retCode);
  if (retCode > 0) {
    //Query was sent OK, 
    if (retCode == HTTP_CODE_OK) {
      // Anqswer is OK Too !
      leds_ok();
      String content = http.getString();
      http.end();
      Serial.println(content);
      //lets try to parse it
      DeserializationError error = deserializeJson(json_return, content);
      // Test if parsing succeeds.
      if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.c_str());
        deserializeJson(json_return, "{error:1}");
        return (-3); // JSON ERROR
      } 
     return (1); 
    } else {
      // Answer is not OK
      leds_hs();
      return (-1); // Return code <>200  
    }
  } else {
    leds_hs();
    // Query raised an error occured
    Serial.print ("[HTTP] Get failed, error :");
    Serial.println(retCode);
    return (-1); // Return code < 0 
     
  }
}

void wifi_connect(){
  // We start by connecting to a access point named with password (as in cofig.h)
  WiFiMulti.addAP(mySSID, myPASSWD);
  Serial.println();
  Serial.println();
  Serial.print("Waiting for WiFi... ");
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  delay(500);
}
